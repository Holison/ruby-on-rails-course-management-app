class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :prerequisite
      t.string :category
      t.string :location
      t.references :coordinator, foreign_key: true
      #t.string :coordinator_name
      t.timestamps
    end
    add_index :courses, [:coordinator_id, :created_at]
  end
end
