class AddAdminToCoordinators < ActiveRecord::Migration[5.1]
  def change
    add_column :coordinators, :admin, :boolean, default: false
  end
end
