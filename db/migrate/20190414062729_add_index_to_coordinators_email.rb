class AddIndexToCoordinatorsEmail < ActiveRecord::Migration[5.1]
  def change
    add_index :coordinators, :email, unique: true 

  end
end
