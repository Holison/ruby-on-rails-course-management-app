class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      #t.string :location
      t.references :course, foreign_key: true

      t.timestamps
    end
    add_index :locations, [:course_id, :created_at]
  end
end
