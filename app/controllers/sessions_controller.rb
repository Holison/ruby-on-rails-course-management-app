class SessionsController < ApplicationController
  def new
  end

  def create
    coordinator = Coordinator.find_by(email: params[:session][:email].downcase)
    if coordinator && coordinator.authenticate(params[:session][:password])
      log_in coordinator
      flash[:success] = 'Log in successfully!!'
      redirect_back_or courses_path
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out
    flash[:success] = 'You have logged out!!!'
    redirect_to root_url
  end
end
