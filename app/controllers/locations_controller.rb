class LocationsController < ApplicationController

  def index
    @locations = Location.all
  end


  def show
    @location = Location.find(params[:id])
  end

  def new
    @location = Location.new()
  end

  def edit
    @location = Location.find(params[:id])
  end

  def create
    @location = Location.new(location_params)
      if @location.save
        flash[:success] = "Location successfully added!"
       redirect_to locations_path
      else
        render :new
      end
  end

    def update
      @location = Location.find(params[:id])
        if @location.update_attributes(location_params)
          redirect_to locations_path
        else
          render 'edit'
        end
    end

    def destroy
      Location.find(params[:id]).destroy
      flash[:success] = "Location deleted"
      redirect_to locations_path
    end

  private
  def location_params
    params.require(:location).permit(:location, :course_ids)
  end

  def find_course
     @course = Course.find(params[:id])
     #@course = current_coordinator.courses.find(params[:id])
  end

end
