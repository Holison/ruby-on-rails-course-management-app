class CoursesController < ApplicationController
before_action :logged_in_coordinator, only: [:create,:new,:show,:edit,:update, :destroy]
#before_action :set_course, only: [:show, :edit, :update, :destroy]

  def index
    @courses = Course.all
  end


  def show
  	#@course = Course.find(params[:id])
    @course = Course.find(params[:id])
  end

  def new
    #@course = @coordinator.course.new
    @course = Course.new()
  end

  def edit
    @course = Course.find(params[:id])
  end

  def create

    @course = current_coordinator.courses.build(course_params)
    if @course.save
      flash[:success] = "Course created!"
      redirect_to current_coordinator
    else
      render 'new'
    end

  end


  def update
    @course = Course.find(params[:id])
    if @course.update_attributes(course_params)
      #if @course.update(params[:id])
        flash[:success] = "Course edited!"
        redirect_to courses_path

      else
        render 'edit'
      end

  end


  def destroy
    #current_coordinator.courses.find(params[:id]).destroy
    Course.find(params[:id]).destroy
    flash[:success] = "Course deleted"
    #redirect_to courses_path
    redirect_back(fallback_location: courses_path)
  end



  private
  def course_params
    params.require(:course).permit(:name, :prerequisite, :description, :image, category_ids: [], location_ids: [])
  end

  def set_course
    @course = Course.find(params[:id])
  end

end
