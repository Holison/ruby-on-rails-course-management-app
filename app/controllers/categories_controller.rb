class CategoriesController < ApplicationController
  

  def index
    @categories = Category.all
  end


  def show
    @category = Category.find(params[:id])
  end

  def new
    @category = Category.new()
  end

  def edit
    @category = Category.find(params[:id])
  end

  def create
    @category = Category.new(category_params)
      if @category.save
        flash[:success] = "Category successfully added!"
       redirect_to categories_path
      else
        render :new
      end
  end

    def update
      @category = Category.find(params[:id])
        if @category.update_attributes(category_params)
          redirect_to categories_path
        else
          render 'edit'
        end
    end

    def destroy
      Category.find(params[:id]).destroy
      flash[:success] = "Category deleted"
      redirect_to categories_path

    end

  private
  def category_params
    params.require(:category).permit(:category, :course_ids)
  end

  def find_course
     @course = Course.find(params[:id])
     #@course = current_coordinator.courses.find(params[:id])
  end


end
