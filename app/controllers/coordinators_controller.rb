class CoordinatorsController < ApplicationController
  before_action :logged_in_coordinator, only: [:index, :edit, :update, :destroy]
  before_action :correct_coordinator,   only: [:edit, :update]
  before_action :admin_coordinator,     only: [:create, :edit, :destroy]
  #before_action :set_coordinator, only: [:show, :edit, :update, :destroy]

  # GET /coordinators
  # GET /coordinators.json
  def index
    @coordinators = Coordinator.all
  end

  # GET /coordinators/1
  # GET /coordinators/1.json
  def show
  	@coordinator = Coordinator.find(params[:id])
  end

  # GET /coordinators/new
  def new
    @coordinator = Coordinator.new
  end

  # GET /coordinators/1/edit
  def edit
    @coordinator = Coordinator.find(params[:id])
  end

  # POST /coordinators
  # POST /coordinators.json
  def create
    @coordinator = Coordinator.new(coordinator_params)
      if @coordinator.save
        log_in @coordinator #remove this to force users to sign in again
        flash[:success] = "Account successfully created!"
       redirect_to @coordinator
      else
        render :new
      end

  end

  # PATCH/PUT /coordinators/1
  # PATCH/PUT /coordinators/1.json
  def update

    @coordinator = Coordinator.find(params[:id])
    if @coordinator.update_attributes(coordinator_params)
      flash[:success] = "Profile updated"
      redirect_to @coordinator
    else
      render 'edit'
    end

  end

  # DELETE /coordinators/1
  # DELETE /coordinators/1.json
  def destroy
    Coordinator.find(params[:id]).destroy
    flash[:success] = "Coordinator Account deleted"
    redirect_to coordinators_url

  end

  private

    def set_coordinator
      @coordinator = Coordinator.find(params[:id])
    end


    def coordinator_params
      # params.require(:coordinator).permit(:name, :image, :course_data => [])
      params.require(:coordinator).permit(:name, :email, :password,:password_confirmation,:course_data => [])
    end

    def admin_coordinator
      @coordinator = Coordinator.find_by(admin: true)
    end

    def correct_coordinator
      @coordinator = Coordinator.find(params[:id])
      redirect_to(root_url) unless current_coordinator?(@coordinator)|| current_coordinator.admin?
    end
end
