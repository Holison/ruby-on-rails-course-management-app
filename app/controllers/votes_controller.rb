class VotesController < ApplicationController
#before_action :find_course
skip_before_action :verify_authenticity_token


    def create

    end

    def upvote
      @course = Course.find(params[:format])

        #flash[:success] = "Happy you like the course :)!"
        if current_coordinator.votes.where(:course_id => @course).present?
          flash[:danger] = "You can only vote once per course!"
          #redirect_to(course_path(@course))
          redirect_back(fallback_location: courses_path)
        else
          @course.votes.upvote.build(coordinator_id: current_coordinator.id)
          @course.save
          flash[:success] = "Thanks for your vote!"
          #redirect_to(course_path(@course))
          redirect_back(fallback_location: courses_path)

        end
    end

    def downvote
        @course = Course.find(params[:format])

        if current_coordinator.votes.where(:course_id => @course).present?
          flash[:danger] = "You can only vote once per course!"
          #redirect_to(course_path(@course))
          redirect_back(fallback_location: courses_path)
        else
         @course.votes.downvote.create(coordinator_id: current_coordinator.id)
         @course.save
         flash[:success] = "Thanks for your vote!"
         #redirect_to(course_path(@course))
         redirect_back(fallback_location: courses_path)
        end

    end


    def destroy
      @course = Course.find(params[:course_id])
      @course.votes.destroy_all
      flash[:success] = "Votes Resetted"
      #redirect_to courses_path
      redirect_back(fallback_location: courses_path)

    end

    private
    def find_course
       @course = Course.find(params[:id])
    end

    def vote_params
     params.require(:vote).permit(:coordinator_id, :course_id, :vote_type)
    end
end
