class ContactsController < ApplicationController

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.valid?
      ContactMailer.contact_message(@contact).deliver_now
      redirect_to "/contacts/new"
      flash[:success] = "We have received your message and will be in touch soon!"
    else
      #flash[:error] = "Missing fields, Please fill accordingly."
      render :new
    end
  end


  private
  def contact_params
      params.require(:contact).permit(:name, :email, :phone, :content)
  end

end
