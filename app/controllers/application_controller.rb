class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  #rescue_from ActionController::RoutingError, :with => :render_not_found
  unless Rails.application.config.consider_all_requests_local
    rescue_from ActionController::RoutingError, :with => :render_not_found
    rescue_from AbstractController::ActionNotFound, :with => :render_not_found
    rescue_from ActiveRecord::RecordNotFound,        :with => :render_not_found
  end

  def render_not_found
    render "errors/not_found", status: 404
  end


  private
    def logged_in_coordinator
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

  #protected

    # def render_server_error
    #   render "shared/500", :status => 500
    # end
end
