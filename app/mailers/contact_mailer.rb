class ContactMailer < ActionMailer::Base
  default to: "sappo414@gmail.com"


  layout "mailer"

  def contact_message(contact)
    @contact = contact
    mail( :from => @contact.email, :subject => "You Have a Message From Your Website", content: @contact.content)

  end
end
