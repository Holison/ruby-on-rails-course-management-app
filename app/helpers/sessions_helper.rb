module SessionsHelper
  def log_in(coordinator)
    session[:coordinator_id] = coordinator.id
  end

  def current_coordinator?(coordinator)
    coordinator == current_coordinator
  end


  def current_coordinator
    if session[:coordinator_id]
      @current_coordinator ||= Coordinator.find_by(id: session[:coordinator_id])
    end
  end

  def logged_in?
    !current_coordinator.nil?
  end

  def log_out
    session.delete(:coordinator_id)
    @current_coordinator = nil
  end

  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
