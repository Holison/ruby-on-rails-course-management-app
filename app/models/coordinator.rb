class Coordinator < ApplicationRecord

  before_save { self.email = email.downcase }
  validates :name, presence: true, length: {minimum: 4}

  #VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  VALID_EMAIL_REGEX = /\A[a-z]+\.[a-z]+@[rmit]+\.[edu]+\.+[au]+$\z/i
  validates :email, presence: true, length: {minimum: 4},
  format: { with: VALID_EMAIL_REGEX, message: 'Registration only open for RMIT Staff' },
  uniqueness: { case_sensitive: false } 

  has_secure_password
  has_many :courses, dependent: :destroy
  #attr_accessor :course_data
  validates :password, presence: true, length: {minimum: 8, message: 'Password must contain at least a lowercase letter, an uppercase letter, a digit, a special character and 8+ characters'} #,allow_nil: false

  has_many :votes, dependent: :destroy

  def Coordinator.digest(string)
      cost = ActiveModel::SecurePassword.min_cost ?
      BCrypt::Engine::MIN_COST :                                     BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
  end



end
