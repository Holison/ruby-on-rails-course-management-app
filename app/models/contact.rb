class Contact
  include ActiveModel::Model
  attr_accessor :name, :email, :phone, :content
  validates :name, :phone, :content, presence: true
  validates :email, :presence => :true,
  :format => { :with => /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i,
    :message => "must be a valid email address"
    }

end
