class Vote < ApplicationRecord
  belongs_to :course
  belongs_to :coordinator
  enum vote_type: [ :upvote, :downvote ]
  validates_uniqueness_of :course_id, scope: :coordinator_id


  def count_like
    self.votes.where(vote_type: 0).size
  end

  def count_dislike
    self.votes.where(vote_type: 1).size
  end


end
