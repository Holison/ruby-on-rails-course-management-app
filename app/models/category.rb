class Category < ApplicationRecord
  has_and_belongs_to_many :courses
  #has_many :locations, through: :courses
  #validates :course_id, presence: true
  attr_accessor :course_id
end
