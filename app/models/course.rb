class Course < ApplicationRecord

  mount_uploader :image, CourseUploader
  belongs_to :coordinator

  validates :name, presence: true, length: {minimum: 10}
  validates :prerequisite, presence: true, length: {minimum: 10}
  validates :description, presence: true, length: {minimum: 30}


  validates :coordinator_id, presence: true
  has_many :votes, dependent: :destroy
  has_and_belongs_to_many :locations
  has_and_belongs_to_many :categories



end
