json.extract! coordinator, :id, :name, :email, :courses, :created_at, :updated_at
json.url coordinator_url(coordinator, format: :json)
