desc 'send email'
task send_email: :environment do
  ContactMailer.contact_message(Contact.last).deliver!
end
