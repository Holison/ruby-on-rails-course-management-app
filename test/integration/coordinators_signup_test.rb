require 'test_helper'

class CoordinatorsSignupTest < ActionDispatch::IntegrationTest

  test "invalid signup information" do
    get signup_path
    assert_no_difference 'Coordinator.count' do
      post coordinators_path, params: { coordinator: { name:  "",
                                         email: "coordinator@invalid",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'coordinators/new'
  end


  test "valid signup information" do
      get signup_path
      assert_difference 'Coordinator.count', 1 do
        post coordinators_path, params: { coordinator: { name:  "Example User",
                                           email: "user@example.com",
                                           password:              "password",
                                           password_confirmation: "password" } }
      end
      follow_redirect!
      assert_template 'coordinators/show'
    end



end
