require 'test_helper'

class ContactTest < ActiveSupport::TestCase

  test 'responds to name, email, phone and content' do
    msg = Contact.new

    assert msg.respond_to?(:name),  'does not respond to :name'
    assert msg.respond_to?(:email), 'does not respond to :email'
    assert msg.respond_to?(:phone), 'does not respond to :phone'
    assert msg.respond_to?(:content),  'does not respond to :content'

  end

  test 'should be valid when all attributes are set' do
    attrs = {
      name: 'stephen',
      email: 'stephen@example.org',
      phone: 03633636,
      content: 'kthnxbai'
    }

    msg = Contact.new attrs
    assert msg.valid?, 'should be valid'
  end

  test 'name, email, phone and content are required by law' do
      msg = Contact.new

      refute msg.valid?, 'Blank Mesage should be invalid'

      assert_match /blank/, msg.errors[:name].to_s
      assert_match /blank/, msg.errors[:email].to_s
      assert_match /blank/, msg.errors[:phone].to_i
      assert_match /blank/, msg.errors[:content].to_s

  end

end
