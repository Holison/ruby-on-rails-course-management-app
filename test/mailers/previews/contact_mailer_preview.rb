class ContactMailerPreview < ActionMailer::Preview
  def contact_message_preview
    @contact = Contact.new(name: 'sam', email: 'sam@gaml.com', phone: 25353, content: 'test')
    ContactMailer.contact_message(@contact)
  end
end
