require 'test_helper'

class ContactMailerTest < ActionMailer::TestCase

  test "contact_message" do
    message = Contact.new name: 'anna',
                          email: 'anna@example.org',
                          phone: 3547474,
                          content: 'hello, how are you doing?'

    email = ContactMailer.contact_message(message)

    assert_emails 1 do
      email.deliver_now
    end

    assert_equal 'Message from Ruby.com', email.subject
    assert_equal ['stephen@example.org'], email.to
    assert_equal ['anna@example.org'], email.from
    assert_match /hello, how are you doing?/, email.content.encoded
  end
end
