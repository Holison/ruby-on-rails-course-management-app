require 'test_helper'

class ContactsControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test "GET new" do
    get new_contact_url
    assert_response :success

      assert_select 'form' do
        assert_select 'input[type=text]'
        assert_select 'input[type=email]'
        assert_select 'input[type=number]'
        assert_select 'textarea'
        assert_select 'input[type=submit]'
      end
  end

  test "POST create" do
      post create_contact_url, params: {
        contact: {
          name: 'cornholio',
          email: 'cornholio@example.org',
          phone: "0456578",
          content: 'hai'
        }
      }

      assert_redirected_to new_contact_url

      follow_redirect!

      assert_match /We have received your message and will be in touch soon!/, response.content
    end

end
