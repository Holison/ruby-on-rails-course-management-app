Rails.application.routes.draw do
  get 'sessions/new'

  resources :coordinators
  root 'main#home'
  get 'main/home'
  get 'courses/index'
  #get 'contacts/new' => 'contacts#new'
  #post 'form' => 'contacts#create'
  resources :contacts
  resources :courses
  resources :coordinators
  resources :locations
  resources :categories

  unless Rails.application.config.consider_all_requests_local
    # having created corresponding controller and action
    match '/404' => 'errors#error_404', via: :all
    #get '/404' => 'errors#error_404', via: :get

  end

  get  '/signup',  to: 'coordinators#new'
  post '/signup',  to: 'coordinators#create'
  get  '/login',   to: 'sessions#new'
  post '/login',   to: 'sessions#create'
  delete '/logout',to: 'sessions#destroy'

  get '/upvote',   to: 'votes#new'
  get '/downvote', to: 'votes#new'
  post '/upvote',   to: 'votes#upvote'
  post '/downvote', to: 'votes#downvote'

  resources :courses do
      resources :votes
  end

  
  # match "*missing" => redirect("errors#error_404"), via: :all
end
