# Load the Rails application.
require_relative 'application'

aws = File.join(Rails.root, 'config', 'initializers','aws.rb')
load(aws) if File.exists?(aws)
# Initialize the Rails application.
Rails.application.initialize!
